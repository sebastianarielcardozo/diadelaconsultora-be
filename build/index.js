'use strict';

require('babel-polyfill');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _video = require('./routes/video.route');

var _video2 = _interopRequireDefault(_video);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var port = process.env.PORT || 3000;

app.use(_express2.default.urlencoded({ limit: "50mb", extended: true }));
app.use(_express2.default.json({ limit: "50mb", extended: true }));

_video2.default.routesConfig(app);

app.get("/", function (req, res) {
  res.status(200).json({ message: "Welcome to Dia del consultor o consultora Natura!" });
});

app.listen(port, function () {
  console.log("\x1b[34m", 'Application listening at port ' + port, "\x1b[0m");
});
//# sourceMappingURL=index.js.map