"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _video = require("../controllers/video.controller");

var _video2 = _interopRequireDefault(_video);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* import AuthMiddleware from "../middlewares/auth.middleware.js";
 */
function routesConfig(app) {
  app.post("/video/", [
  //AuthMiddleware.validJWTNeeded,
  _video2.default.createVideo]);
}

exports.default = { routesConfig: routesConfig };
//# sourceMappingURL=video.route.js.map