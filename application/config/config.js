module.exports = {
  port: process.env.PORT || 5000,
  jwt_secret: process.env.JWT_SECRET || "zOsgBpJYRRbC72uoVYpsoQPzFgDIh0YX",
  jwt_expiration_in_seconds: process.env.JWT_EXPIRATION || 36000,
};
