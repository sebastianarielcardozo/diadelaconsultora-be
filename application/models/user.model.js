import mongoose from "../services/mongose.service";
import Video from "./video.model";

let Schema = mongoose.Schema;

let Users = Schema({
  cod_consultora: String,
  nombre: String,
});

const User = mongoose.model("Users", Users);

const findByCodConsultora = (cod_consultora, pais) =>
  User.findOne({ cod_consultora, pais });

const findById = async (_id) => {
  const user = await User.findOne({ _id });
  const video = await Video.findByUserVideo(_id);

  return { user, video };
};

export default { findById, User, findByCodConsultora };
