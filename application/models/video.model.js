import mongoose from "../services/mongose.service";

let Schema = mongoose.Schema;

let Videos = Schema(
  {
    user: { type: Schema.ObjectId, ref: "User" },
    url: { type: String },
    video: { type: String },
    youtube: { type: String },
    placeImage: { type: String },
    faceImage: { type: String },
    rolImage: { type: String },
    friendImage: { type: String },
    nickname: { type: String },
    firstText: { type: String },
    secondText: { type: String },
    lastText: { type: String },
    bases: { type: Boolean },
    pais: { type: String },
  },
  { timestamps: true }
);

let Video = mongoose.model("Video", Videos);

const create = (video) => new Video(video).save();
const findByUserVideo = async (user) => {
  return Video.findOne({ user });
};

const update = (video, url) =>
  Video.findOneAndUpdate({ video }, { url }, { new: true });

/* const findAll = () => Video.find().sort({ createdAt: -1 });

 */

const findAll = async (pais) => {
  const vid = await Video.find({ pais }).sort({ createdAt: -1 }).exec();

  return vid;
};

const findShare = async (_id) => {
  return Video.findOne({ _id });
};

export default { Video, create, findByUserVideo, update, findAll, findShare };
