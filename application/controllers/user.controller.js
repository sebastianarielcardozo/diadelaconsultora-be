import User from "../models/user.model.js";
import Jwt from "jsonwebtoken";
import config from "../config/config.js";

const isUserMatch = async (req, res, next) => {
  const {
    body: { cod_consultora, pais },
  } = req;

  var user;

  if (!cod_consultora || !pais) {
    return res
      .status(400)
      .send({ error: "El codigo de consultora es requerido." });
  }

  try {
    user = await User.findByCodConsultora(cod_consultora, pais);
    if (!user) {
      return res.status(400).send({ error: "El codigo ingresado no existe." });
    }
    req.body.user = user;
    next();
  } catch (error) {
    return res.status(400).send({ error });
  }
};

const login = async (req, res) => {
  const {
    body: { user },
  } = req;

  try {
    const token = Jwt.sign({ user }, config.jwt_secret, {
      expiresIn: config.jwt_expiration_in_seconds,
    });

    res.status(200).send({
      token,
    });
  } catch (err) {
    res.status(500).send({ errors: err });
  }
};

const getUserByToken = async (req, res) => {
  const {
    jwt: { _id },
  } = req;

  try {
    let user = await User.findById(_id);
    res.status(200).send(user);
  } catch (error) {
    res.status(400).send({ error });
  }
};

export default { login, isUserMatch, getUserByToken };
