import HttpStatus from "http-status-codes";
import axios from "axios";
import dotenv from "dotenv";
var http = require("http");
var request = require("request");

import Video from "../models/video.model";

dotenv.config();

let apiUrl = "https://api.shotstack.io/stage";

if (!process.env.SHOTSTACK_KEY) {
  console.log(
    "API Key is required. Set using: export SHOTSTACK_KEY=your_key_here"
  );
  process.exit(1);
}

if (process.env.SHOTSTACK_HOST) {
  apiUrl = process.env.SHOTSTACK_HOST;
}

const template = (
  nickname,
  firstText,
  placeImage,
  faceImage,
  rolImage,
  friendImage,
  pais
) => ({
  timeline: {
    background: "#e2c12d",
    fonts: [
      {
        src: "https://assets-diadelconsultornatura.s3.sa-east-1.amazonaws.com/HELVETICANOWDISPLAY_0.TTF",
      },
    ],
    soundtrack: {
      src: "https://assets-diadelconsultornatura.s3.sa-east-1.amazonaws.com/musica.mp3",
      effect: "fadeOut",
    },

    tracks: [
      {
        clips: [
          {
            asset: {
              type: "image",
              src: "https://assets-diadelconsultornatura.s3.sa-east-1.amazonaws.com/LOGO+NATURA+MOSCA+v2.png",
            },
            start: 0,
            length: 25,
            fit: "none",
            position: "topRight",
          },
        ],
      },
      {
        clips: [
          {
            asset: {
              type: "title",
              text: ` Cada Consultor y Consultora de Belleza Natura \n es una persona apasionada por el poder transformador de los cosméticos `,
              style: "subtitle",
              color: "#000000",
              size: "x-small",
              background: "#e2c12d",
              position: "bottom",
              offset: {
                y: -0.4,
              },
            },
            start: 0.2,
            length: 4.8,
          },
          {
            asset: {
              type: "title",
              text: ` ... y las relaciones `,
              style: "subtitle",
              color: "#000000",
              size: "x-small",
              background: "#e2c12d",
              position: "bottom",
              offset: {
                y: -0.4,
              },
            },
            start: 5.2,
            length: 4.8,
          },
          {
            asset: {
              type: "title",
              text: ` ${nickname} transforma su mundo día a día `,
              style: "subtitle",
              color: "#000000",
              size: "x-small",
              background: "#e2c12d",
              position: "bottom",
              offset: {
                y: -0.4,
              },
            },
            start: 10.2,
            length: 4.8,
          },
          {
            asset: {
              type: "title",
              text: ` "${firstText}" `,
              style: "subtitle",
              color: "#000000",
              size: "x-small",
              background: "#e2c12d",
              position: "bottom",
              offset: {
                y: -0.4,
              },
            },
            start: 15.2,
            length: 4.8,
          },
          {
            asset: {
              type: "title",
              text: "  ¡Gracias por ser parte de nuestra red transformadora \n y construir un mundo más bonito! ",
              style: "subtitle",
              color: "#000000",
              size: "x-small",
              background: "#e2c12d",
              position: "bottom",
              offset: {
                y: -0.4,
              },
            },
            start: 20.2,
            length: 4.8,
          },
        ],
      },

      {
        clips: [
          {
            asset: {
              type: "image",
              src: `https://assets-diadelconsultornatura.s3.sa-east-1.amazonaws.com/VIDEO+ENTREGABLE+1er+frame_${pais}.jpg`,
            },
            start: 0,
            length: 5,
            fit: "contain",
            transition: {
              out: "fade",
            },
          },
          {
            asset: {
              type: "image",
              src: friendImage,
            },
            start: 5,
            length: 5,
            fit: "contain",
            transition: {
              in: "fade",
              out: "fade",
            },
            effect: "zoomIn",
          },
          {
            asset: {
              type: "image",
              src: placeImage,
            },
            start: 10,
            length: 5,
            fit: "contain",
            transition: {
              in: "fade",
              out: "fade",
            },
            effect: "zoomIn",
          },
          {
            asset: {
              type: "image",
              src: rolImage,
            },
            start: 15,
            length: 5,
            fit: "contain",
            transition: {
              in: "fade",
              out: "fade",
            },
            effect: "zoomIn",
          },
          {
            asset: {
              type: "image",
              src: faceImage,
            },
            start: 20,
            length: 5,
            fit: "contain",
            transition: {
              in: "fade",
              out: "fade",
            },
            effect: "zoomIn",
          },
        ],
      },
    ],
  },
  output: {
    format: "mp4",
    /*     resolution: "mobile", */
    aspectRatio: "1:1",
    size: {
      width: 800,
      height: 800,
    },
  },
  /*   callback: callback + "upload", */
});

const renderVideo = async (json) => {
  const response = await axios({
    method: "post",
    url: apiUrl + "/render",
    headers: {
      "x-api-key": process.env.SHOTSTACK_KEY,
      "content-type": "application/json",
    },
    data: json,
  });

  return response.data;
};

const pollVideoStatus = async (uuid) => {
  const response = await axios({
    method: "get",
    url: apiUrl + "/render/" + uuid,
    headers: {
      "x-api-key": process.env.SHOTSTACK_KEY,
      "content-type": "application/json",
    },
  });

  if (response.data.response.status === "done") {
    return response.data.response.url;
  } else if (response.data.response.status === "failed") {
    return response.data.response.error;
  } else {
    await new Promise(r => setTimeout(r, 3000));
    return await pollVideoStatus(uuid);
  }
};

const createVideo = async (req, res) => {
  const {
    jwt: { _id },
    body: {
      nickname,
      firstText,
      secondText,
      lastText,
      bases,
      placeImage,
      faceImage,
      rolImage,
      friendImage,
      pais,
    },
  } = req;

  try {
    const {
      response: { id },
    } = await renderVideo(
      JSON.stringify(
        template(
          nickname,
          firstText,
          placeImage,
          faceImage,
          rolImage,
          friendImage,
          pais
        )
      )
    );

    let result = await Video.create({
      user: _id,
      video: id,
      nickname,
      firstText,
      secondText,
      lastText,
      placeImage,
      faceImage,
      rolImage,
      friendImage,
      bases,
      pais,
      url: await pollVideoStatus(id),
    });

    /*    res.setHeader("content-disposition", "attachment; filename=logo.mp4");
    request(result.url).pipe(res); */

    res.status(HttpStatus.OK).send(result);
  } catch (err) {
    console.error(err);
  }
};

const getAllVideo = async (req, res) => {
  const {
    jwt: { _id },
    query: { pais },
  } = req;

  try {
    let result = await Video.findAll(pais);

    res.status(HttpStatus.OK).send(result);
  } catch (err) {
    console.error(err);
  }
};


const getShare = async (req, res) => {
  const {
    query: { video },
  } = req;

  try {
    let result = await Video.findShare(video);

    res.status(HttpStatus.OK).send(result);
  } catch (err) {
    console.error(err);
  }
};

export default { createVideo, getAllVideo, getShare };
