import mongoose from "mongoose";

let count = 0;

const options = {
  autoIndex: false, // Don't build indexes

  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  user: "",
  pass: "",
};

const connectWithRetry = () => {
  console.log("MongoDB connection with retry");
  mongoose
    .connect(
      "mongodb+srv://natura:natura_sinfin_2021@cluster0.zo36n.mongodb.net/diadelconsultornatura?retryWrites=true&w=majority",
      options
    )
    .then(() => {
      /*   mongoose
    .connect(
      "mongodb://localhost:27017/proyectox",
      options
    ) 
    .then(() => {*/
      console.log("\x1b[32m", "MongoDB is connected", "\x1b[0m");
    })
    .catch((err) => {
      console.log(err);
      console.log(
        "MongoDB connection unsuccessful, retry after 5 seconds. ",
        ++count
      );
      setTimeout(connectWithRetry, 5000);
    });
};

connectWithRetry();

export default mongoose;
