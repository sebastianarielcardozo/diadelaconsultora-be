let Jwt = require("jsonwebtoken");
let config = require("../config/config");

const validJWTNeeded = (req, res, next) => {

  const { token } = req;

  if (token) {
    Jwt.verify(token, config.jwt_secret, (err, decode) => {
      if (err) {
        return res.status(401).send({
          err,
        });
      }
      req.jwt = decode.user;
      next();
    });
  } else {
    return res.status(401).send();
  }
};

export default  { validJWTNeeded };