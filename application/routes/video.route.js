import VideoController from "../controllers/video.controller";
import AuthMiddleware from "../middlewares/auth.middleware.js";

function routesConfig(app) {
  app.post("/video", [
    AuthMiddleware.validJWTNeeded,
    VideoController.createVideo,
  ]);
  app.get("/video", [
    AuthMiddleware.validJWTNeeded,
    VideoController.getAllVideo,
  ]);
  app.get("/share", [
    VideoController.getShare,
  ]);
}

export default { routesConfig };