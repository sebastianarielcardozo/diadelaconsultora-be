import UserController from "../controllers/user.controller.js";

import AuthMiddleware from "../middlewares/auth.middleware.js";

function routesConfig(app) {
  app.post("/login", [UserController.isUserMatch, UserController.login]);
  app.post("/me", [
    AuthMiddleware.validJWTNeeded,
    UserController.getUserByToken,
  ]);
}

export default { routesConfig };
