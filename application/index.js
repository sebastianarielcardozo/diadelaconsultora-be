import "core-js";
import "regenerator-runtime/runtime";
import express from "express";
import VideoRouter from "./routes/video.route";
import UserRouter from "./routes/user.route";
import bearerToken from "express-bearer-token";
import cors from "cors";

var app = express();

app.use(cors());

var port = process.env.PORT || 80;

app.use(express.urlencoded({ limit: "50mb", extended: true }));
app.use(express.json({ limit: "50mb", extended: true }));
app.use(bearerToken());

app.get("/", (req, res) => {
  res
    .status(200)
    .json({ message: "Welcome to Dia del consultor o consultora Natura! v.1" });
});

VideoRouter.routesConfig(app);
UserRouter.routesConfig(app);

app.listen(port, () => {
  console.log("\x1b[34m", `Application listening at port ${port}`, "\x1b[0m");
});
